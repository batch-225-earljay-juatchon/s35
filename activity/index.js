const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3008;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://earljay0606:mahalkosijhen06@cluster0.pfnycvo.mongodb.net/set?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log("Connected to MongoDB!"));

const userSchema = new mongoose.Schema({ 
	username : String,
	password : String,
});
 
const User = mongoose.model("User", userSchema);

app.post('/signup', (req,res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("User already exist! Please try again!");
		} else {
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered!");
				}
			})
		}
	})
});

app.listen(port, () => console.log(`Server running at port ${port}!`))

